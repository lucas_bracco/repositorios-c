#include <stdio.h>
#include <math.h>

float pitagoras (float, float);

int main (){
    float h=0,c1=0,c2=0;
     printf("Ingrese el valor de Cateto 1:\n");
    scanf("%f",&c1);
    printf("Ingrese el valor de Cateto 2:\n");
    scanf("%f",&c2);
    h=pitagoras(c1,c2);
    printf("El valor del Cateto 1 es: %f\n",c1);
    printf("El valor del Cateto 2 es: %f\n",c2);
    printf("El valor de la Hipotenusa es: %f\n",h);
    return 0;
}

float pitagoras (float a, float b){
float hip;
hip=sqrt(pow(a,2)+pow(b,2));
return hip;
}
