#include <stdio.h>
int cargar (int v[],int n);
int main(){
    int vector[10]={3,13,49,5,47,8,2,1,16,4}, orden[10];

    for(int i=0;i<10;i++){
        orden[i]=cargar(vector,i);
    }
    printf("Vector Desordenado:\t");
    for(int i=0;i<10;i++){
        printf(" %d ",vector[i]);
    }
    printf("\nVector Ordenado:\t");
    for(int i=0;i<10;i++){
        printf(" %d ",orden[i]);
    }
    return 0;
}

int cargar(int v[],int n){
    int aux;
    for(int i=0;i<10;i++){
        for(int j=0;j<10;j++){
            if(v[i]<v[j]){
                aux=v[i];
                v[i]=v[j];
                v[j]=aux;
            }
        }
    }
    return v[n];
}