#include<stdio.h>
#include<stdint.h>//Contiene int8_t...int64_t Enteros con determinado tamaño.
#include<stdbool.h>//Contiene bool, true y false
#include<math.h>//Contiene Funciones Matemáticas, Ej: cos, sen, pi, etc.

int main(){
    //FUNCIONES STDINT.H
    int8_t a;
    int16_t b;
    int32_t c;
    int64_t d;
    printf("a: %d byte\nb: %d bytes\nc: %d bytes\nd: %d bytes\n",sizeof(a),sizeof(b),sizeof(c),sizeof(d));
    
    //FUNCIONES STDBOOL.H
    printf("--------------------------------------------\n");
    bool bo=false;
    if (bo==false)
    {
        printf("La Variable bo es Falso\n");
    }else
    {
        printf("La Variable bo es Verdadero\n");
    }

    //FUNCIONES MATH.H
    printf("--------------------------------------------\n");
    float p=M_PI;
    printf("Valor de Pi: %f\n",p);
    printf("Valor de Pi/2: %f\n",M_PI_2);
    printf("Valor de Pi/4 %f\n",M_PI_4);
    printf("Valor de 1/Pi: %f\n",M_1_PI);
    printf("Raiz Cuadrada de 2: %f\n",sqrt(2));
    printf("Potencia 2^4: %f\n",pow(2,4));
    printf("Potencia 2^4: %.0f\n",pow(2,4));//Con %.0f Omite la parte decimal y muestra solo la entera.
    printf("Potencia 2^4: %d\n",(int)pow(2,4));//(int) convierte la funcion o variable en un int.

    return 0;
}