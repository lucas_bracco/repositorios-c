#include <stdio.h>
void cargar (int[]);
void mostrar (int[]);
float promedio (int[]);
int menor(int[]);
int mayor(int[]);
int main(){
    int v[4];
    cargar(v);
    mostrar(v);
    printf("Promedio: %.2f\n",promedio(v));
    printf("El Mayor: %d\n",mayor(v));
    printf("El Menor: %d\n",menor(v));
    return 0;
}

void cargar (int a[]){
    int i;
    for(i=0;i<5;i++){
        printf("Ingrese un numero entero.\n");
        scanf("%d",&a[i]);
    }
}

void mostrar (int a[]){
    int i;
    printf("Vector: ");
    for(i=0;i<5;i++){
        printf("%d ",a[i]);
    }
}

float promedio (int a[]){
    int i,sum=0;
    for(i=0;i<5;i++){
        sum+=a[i];
    }
    return sum/5.0;
}

int menor(int a[]){
    int i,m;
    for(i=0;i<5;i++){
        if(i==0){
            m=a[i];
        }else if(m>a[i]){
            m=a[i];
        }
        
    }
    return m;
}

int mayor(int a[]){
    int i,m;
    for(i=0;i<5;i++){
        if(i==0){
            m=a[i];
        }else if(m<a[i]){
            m=a[i];
        }
        
    }
    return m;
}