//calcular quien es mas alto, mas bajo, mas pesado, mas liviano.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

typedef struct
{
    char nombre[50];
    float peso;     //peso en Kg.
    float altura;   //altura en metro.
}Personas;

void mostrar_personas(Personas a[], float b[]);
void comparacion(Personas a[],int b);

int main(){
    Personas p[3];
    float imc[3];

    //CARGAR VECTOR p
    for(int i=0;i<3;i++){
    printf("Ingrese el nombre de la persona:\n");
    gets(p[i].nombre);
    
    printf("Ingrese el peso en Kg. de la persona:\n");
    scanf("%f", &p[i].peso);
    fflush(stdin);//sirve para limpiar el bufer de entrada.
    printf("Ingrese la altura en m. de la persona:\n");
    scanf("%f", &p[i].altura);
    fflush(stdin);//sirve para limpiar el bufer de entrada.
    imc[i] = p[i].peso / (p[i].altura * p[i].altura);
    }
    system("cls");

    //FUNCION PARA MOSTRAR EL VECTOR.
    mostrar_personas(p,imc);

    system("pause");
    system("cls");

    //FUNCION PARA COMPARAR.
    comparacion(p,5);

    return 0;
}

void mostrar_personas(Personas a[], float b[]){
    for(int i=0;i<3;i++){
    printf("Nombre: %s\nAltura: %.2f metros.\nPeso: %.3fKg.\nIMC: %.2f\n",a[i].nombre,a[i].altura,a[i].peso,b[i]);
    }
}

void comparacion(Personas a[],int b){
    Personas aux;
//----------------------PERSONA MAS ALTA--------------------------------------------------------
    for(int i=0;i<2;i++){
        if(a[i].altura > a[i+1].altura){
            aux.altura = a[i].altura;
            aux.peso = a[i].peso;
            strcpy(aux.nombre,a[i].nombre);
        }else{
            aux.altura = a[i+1].altura;
            aux.peso = a[i+1].peso;
            strcpy(aux.nombre,a[i+1].nombre);
        }
    }
    printf("La Persona mas alta es: %s, mide: %.2f\n",aux.nombre,aux.altura);
//----------------------PERSONA MAS BAJA--------------------------------------------------------
    for(int i=0;i<2;i++){
        if(a[i].altura < a[i+1].altura){
            aux.altura = a[i].altura;
            aux.peso = a[i].peso;
            strcpy(aux.nombre,a[i].nombre);
        }else{
            aux.altura = a[i+1].altura;
            aux.peso = a[i+1].peso;
            strcpy(aux.nombre,a[i+1].nombre);
        }
    }
    printf("La Persona mas baja es: %s, mide: %.2f\n",aux.nombre,aux.altura);
//------------------------------------------------------------------------------
    for(int i=0;i<2;i++){
        if(a[i].peso > a[i+1].peso){
            aux.altura = a[i].altura;
            aux.peso = a[i].peso;
            strcpy(aux.nombre,a[i].nombre);
        }else{
            aux.altura = a[i+1].altura;
            aux.peso = a[i+1].peso;
            strcpy(aux.nombre,a[i+1].nombre);
        }
    }
    printf("La Persona mas pesada es: %s, pesa: %.2f\n",aux.nombre,aux.peso);
//------------------------------------------------------------------------------
    for(int i=0;i<2;i++){
        if(a[i].peso < a[i+1].peso){
            aux.altura = a[i].altura;
            aux.peso = a[i].peso;
            strcpy(aux.nombre,a[i].nombre);
        }else{
            aux.altura = a[i+1].altura;
            aux.peso = a[i+1].peso;
            strcpy(aux.nombre,a[i+1].nombre);
        }
    }
    printf("La Persona mas liviana es: %s, pesa: %.2f\n",aux.nombre,aux.peso);
}