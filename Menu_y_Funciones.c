#include <stdio.h>
#include <stdlib.h>

float suma (float, float);
float resta (float, float);
float multipli (float, float);
float dividir (float, float);

int main (){
    int op;
    float n1,n2;
    do
    {
        printf("Elija una opcion:\n1-Suma.\n2-Resta.\n3-Multiplicacion.\n4-Division.\n0-Salir.\n");
        scanf("%d",&op);
        switch (op)
        {
        case 1:
            system ("cls");
            printf("Ingrese dos numeros separados por ','.\n");
            scanf("%f,%f",&n1,&n2);
            printf("%0.2f\n",suma(n1,n2));
            system("pause");
            break;
        case 2:
            system ("cls");
            printf("Ingrese dos numeros separados por ','.\n");
            scanf("%f,%f",&n1,&n2);
            printf("%0.2f\n",resta(n1,n2));
            system("pause");
            break;
        case 3:
            system ("cls");
            printf("Ingrese dos numeros separados por ','.\n");
            scanf("%f,%f",&n1,&n2);
            printf("%0.2f\n",multipli(n1,n2));
            system("pause");
            break;
        case 4:
            system ("cls");
            printf("Ingrese dos numeros separados por ','.\n");
            scanf("%f,%f",&n1,&n2);
            printf("%0.2f\n",dividir(n1,n2));
            system("pause");
            break;
        case 0:
            system ("cls");
            printf("Fin Programa.\n");
            break;
        }
    } while (op!=0);
    return 0;
}

float suma (float a, float b){
    float suma;
    suma = a + b;
    return suma;
}

float resta (float a, float b){
    float resta;
    resta = a - b;
    return resta;
}

float multipli (float a, float b){
    float m;
    m = a * b;
    return m;
}

float dividir (float a, float b){
    float m;
    m = a / b;
    return m;
}