#include <stdio.h>
#include <stdlib.h>
//PROTOTIPO DE LA FUNCIÓN.
float deter(int[3][3]);
//VARIABLES GLOBALES.
int i,j;

int main(){
    //DECLARACIÓN DE LA MATRIZ.
    int matriz[3][3];
    
    //CARGA LA MATRIZ 3X3.
    for(i=0;i<3;i++){
        for(j=0;j<3;j++){
            printf("Ingrese el valor de la matriz [%d][%d].\n",i,j);
            scanf("%d",&matriz[i][j]);
            system("cls");
        }
    }

    system("cls");

    //MUESTRA LA MATRIZ 3X3 CARGADA.
    for(i=0;i<3;i++){
        for(j=0;j<3;j++){
            printf("[%d]",matriz[i][j]);
        }
        printf("\n");
    }

    system("pause");
    system("cls");

    //LLAMA A LA FUNCION.
    deter(matriz);
    return 0;
}

//FUNCION.
float deter(int m[3][3]){
int det=0,ma[5][3];

//CREA LA MATRIZ DE 5X3.
for(i=0;i<3;i++){
    for(j=0;j<3;j++){
        ma[i][j]=m[i][j];
        if(i<2){
            ma[i+3][j]=m[i][j];
        }
    }
}

//MUESTRA LA MATRIZ 5X3.
for(i=0;i<5;i++){
    for(j=0;j<3;j++){
         printf("[%d]",ma[i][j]);
    }
    printf("\n");
}  

//CALCULA EL DETERMINANTE DE LA MATRIZ.  
for(i=0;i<3;i++){
    j=0;
    det=det+(ma[i][j]*ma[i+1][j+1]*ma[i+2][j+2])-(ma[i][j+2]*ma[i+1][j+1]*ma[i+2][j]);
}

//SI EL DETERMINANTE ES 0 MUESTRA MENSAJE.
if(det==0){
    printf("\nLa Matriz ingresada es singular.\n");
}

printf("Determinante: %d\n",det);

}