//CARGAR UNA PALABRA EN UN VECTOR Y MOSTRARLA AL REVES.
#include <stdio.h>

int main(){
    char palabra[5];
    printf("Ingrese una palabra de 5 letras.\n");
    scanf("%s",&palabra);
    for(int i=4;i>=0;i--){
        printf("%c",palabra[i]);
    }
    printf("\n");
}