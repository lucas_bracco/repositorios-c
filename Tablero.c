#include <stdio.h>

int main(){
    char tablero[8][8];
    for(int i=0;i<8;i++){
        for(int j=0;j<8;j++){
            if((i + j) % 2 == 0){
                tablero[i][j] = ' ';
            }else
            {
                tablero[i][j] = '#';
            }
            
        }
    }

    for(int i=0;i<8;i++){
        for(int j=0;j<8;j++){
            printf("[%c]", tablero[i][j]);            
        }
        printf("\n");
    }
    return 0;
}