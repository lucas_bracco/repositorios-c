Proceso Programacion_Trabajo_Practico_N2
	Definir op,aux,i,id,pass,matriz Como Entero;
	Definir reg Como Logico;
	Dimension reg[4,2];
	Dimension matriz[4,6];
	matriz[0,0]<-1000;
	matriz[0,1]<-12345678;
	matriz[1,0]<-1001;
	matriz[1,1]<-11119999;
	matriz[2,0]<-1002;
	matriz[2,1]<-87654321;
	matriz[3,0]<-1003;
	matriz[3,1]<-11111111;
	reg[0,0]<-Falso;
	reg[0,1]<-Falso;
	reg[1,0]<-Falso;
	reg[1,1]<-Falso;
	reg[2,0]<-Falso;
	reg[2,1]<-Falso;
	reg[3,0]<-Falso;
	reg[3,1]<-Falso;
	Repetir
		Escribir "Elija una de las Siguientes opciones.";
		Escribir "1-Registrar Ingreso.";
		Escribir "2-Registrar Egreso.";
		Escribir "3-Salir.";
		leer op;
		Segun op Hacer
			1:
				Escribir "Ingrese su N�mero de Legajo.";
				leer id;
				Escribir "Ingrese su Contrase�a.";
				leer pass;
				Para i<-0 Hasta 3 Con Paso 1 Hacer
					aux<-0;
					Si (matriz[i,0]=id & matriz[i,1]=pass) Entonces
						Escribir "Marcar Hora de Ingreso (sin minutos).";
						leer matriz[i,2];
						Escribir "Marcar Minutos.";
						leer matriz[i,3];
						reg[i,0]<-Verdadero;
						i<-9;
						aux<-1;
					FinSi
				FinPara
				si (aux=0) Entonces
					Escribir "El Legajo y/o la Contrase�a no son correctos.";
				FinSi
			2:
				Escribir "Ingrese su N�mero de Legajo.";
				leer id;
				Escribir "Ingrese su Contrase�a.";
				leer pass;
				Para i<-0 Hasta 3 Con Paso 1 Hacer
					aux<-0;
					Si (matriz[i,0]=id & matriz[i,1]=pass) Entonces
						Escribir "Marcar Hora de Egreso (sin minutos).";
						leer matriz[i,4];
						Escribir "Marcar Minutos.";
						leer matriz[i,5];
						reg[i,1]<-Verdadero;
						i<-9;
						aux<-1;
					FinSi
				FinPara
				si (aux=0) Entonces
					Escribir "El Legajo y/o la Contrase�a no son correctos.";
				FinSi
			3:
				Escribir "Ingrese su n�mero de legajo.";
				leer id;
				Para i<-0 Hasta 3 Con Paso 1 Hacer
					aux<-0;
					Si (matriz[i,0]=id) Entonces
						aux<-1;
						Si reg[i,0]=Falso Entonces
							Escribir "No marc� el Ingreso.";
							op<-0;
						FinSi
						Si reg[i,1]=Falso Entonces
							Escribir "No marc� el Egreso.";
							op<-0;
						FinSi
						i<-9;
					FinSi
				FinPara
				si (aux=0) Entonces
					Escribir "El Legajo no existe.";
					op<-0;
				FinSi
		FinSegun
	Hasta Que op=3;
	Para i<-0 Hasta 3 Con Paso 1 Hacer
		Si (reg[i,0] = Verdadero && reg[i,1]=Verdadero) Entonces
			Escribir "Legajo: ",matriz[i,0]," Hora de Ingreso: ",matriz[i,2],":",matriz[i,3],"Hs. Hora de Egreso: ",matriz[i,4],":",matriz[i,5],"Hs.";
		FinSi
	FinPara
FinProceso
