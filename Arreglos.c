#include <stdio.h>

int main(){
    int i,v[20];
    v[20]={}; //DE ESTA FORMA SE INICIALIZA EN 0 TODOS LOS ESPACIOS DEL VECTOR.

    //CARGAR ARREGLO.
    for(i=0;i<20;i++){
    v[i]=i+1;
    }
    
    //MOSTRAR ARREGLO.
    for(i=0;i<20;i++){
        printf("Arreglo indice %d = %d.\n",i,v[i]);
    }

    return 0;
}