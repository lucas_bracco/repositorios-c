#include <stdio.h>

int main(){
    int palabras=0,i=0,acc=0;
    char cadena[90];
    printf("Ingresa un texto.\n");
    gets(cadena);
    while(cadena[i] != 0){

        //CUANDO EL CARACTER ES (a...z) o (A...Z) IGUALA EL acc EN 1 PARA LUEGO SUMARLO.
        if((64 < cadena[i] && cadena[i] < 91) || (96 < cadena[i] && cadena[i] < 123)){
            acc=1;
        }
        //CADA VEZ QUE SE ENCUENTRA CON UN ESPACIO O CARACTER NULL SUMA LAS VARIABLES acc y palabras.
        if(cadena[i] == 32 || cadena[i+1] == 0){
            palabras += acc;
            acc = 0;
        }
        i++;
    }
    printf("Cantidad de palabras: %d.\n",palabras);
    return 0;
}