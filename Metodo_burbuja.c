#include <stdio.h>

void masrepetido(int v[], int n);

int main(){
    int vector[20]={};
    for(int i=0;i<20;i++){
        printf("Ingrese un numero.\n");
        scanf("%d",&vector[i]);
    }
    masrepetido(vector,20);
    return 0;
}

void masrepetido (int v[], int n){
    int acc=0,aux=0,repit;
    for(int i=0;i<n-1;i++){
        for(int j=0;j<n;j++){
            if(v[i]==v[j]){
                acc++;
            }
        }
        if(acc>aux){

            repit=v[i];
            aux=acc;
        }
        acc=0;     
    }
    printf("El numero que mas se repite es: %d\nVeces que se repite: %d\n",repit,aux);
}