#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

void cargar(int *p, int *q);

int main(){
    int a, b, c, *pa, *pb;

    pa = &a;//pa -> a
    pb = &b;//pb -> b

    cargar(pa,pb);   

    c = *pa + *pb;

    printf("Valor de a = %d\nValor de b = %d\nSuma de a + b = %d\n",*pa,*pb,c);

    return 0;
}

void cargar(int *p, int *q){
    printf("Ingrese el valor de a:\n");
    scanf("%d", p);

    printf("Ingrese el valor de b:\n");
    scanf("%d", q); 
}
