#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//FUNCIONES PROTOTIPO
void cargar ();
void mostrar ();
int cont_cero ();
int cont_positivo ();
int cont_negativo ();

//VARIABLES GLOBALES
int n,v[999];

int main(){
    printf("Ingrese un numero entero para definir el tamanio de su vector.\n");
    scanf("%d",&n);
    cargar(v,n);
    mostrar(v,n);
    printf("Cantidad de Ceros:\t%d\nNumeros Positivos:\t%d\nNumeros Negativos:\t%d\n",cont_cero(v,n),cont_positivo(v,n),cont_negativo(v,n));
    return 0;
}

void cargar (){
    int i;
    srand(time(0));
    for(i=0;i<n;i++){
        v[i]=rand()%41-20; //CALCULA EL RESTO DE RAND() DIVIDIDO EN 41 Y LUEGO SE LE RESTA 20.
    }
}

void mostrar (){
    int i;
    printf("Vector:\n");
    for(i=0;i<n;i++){
        printf("%d\t%d\n",i,v[i]);
    }
}

//FUNCION PARA CONTAR CEROS EN EL VECTOR
int cont_cero (){
    int i,c=0;
    for(i=0;i<n;i++){
        if(v[i]==0){ //SI EL NUMERO GUARDADO EN EL VECTOR ES IGUAL A CERO, AUMENTA EN 1 EL CONTADOR "c".
            c++;
        }
    }
    return c;
}

//FUNCION PARA CONTAR POSITIVOS EN EL VECTOR
int cont_positivo (){
    int i,c=0;
    for(i=0;i<n;i++){
        if(v[i]>0){ //SI EL NUMERO GUARDADO EN EL VECTOR ES MAYOR A CERO, AUMENTA EN 1 EL CONTADOR "c".
            c++;
        }
    }
    return c;
}

//FUNCION PARA CONTAR NEGATIVOS EN EL VECTOR
int cont_negativo (){
    int i,c=0;
    for(i=0;i<n;i++){ //SI EL NUMERO GUARDADO EN EL VECTOR ES MENOR A CERO, AUMENTA EN 1 EL CONTADOR "c".
        if(v[i]<0){
            c++;
        }
    }
    return c;
}