#include <stdio.h>

void ordenar ();

int v[20]={};

int main(){
    for(int i=0;i<20;i++){
        printf("Ingrese el valor de la posicion %d.\n",i);
        scanf("%d",&v[i]);
    }
    ordenar();
    return 0;
}

void ordenar (){
    int aux;
    for(int i=0;i<20;i++){
        for(int j=0;j<20;j++){
            if(v[i]<v[j]){
                aux=v[i];
                v[i]=v[j];
                v[j]=aux;
            }
        }
    }
    printf("Vector ordenado de mayor a menor.\n");
    for(int i=0;i<20;i++){
        printf("Indice %d = %d\n",i,v[i]);
    }
}