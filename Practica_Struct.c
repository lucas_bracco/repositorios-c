#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//ESTRUCTURAS.

typedef struct{
    char nombre[20];
    char director[20];
    char descripcion[100];
}Departamentos;

typedef struct{
    char nombre[20];
    char direccion[50];
    int tel;
    Departamentos dpto;
}Profesores;

typedef struct{
    char nombre[50];
    char nivel[20];
    char descripcion[100];
    Profesores prof;
}Cursos;

int main(){
    Cursos c;
    //CARGAR LAS ESTRUCTURAS.
    strcpy(c.nombre, "Programacion");
    strcpy(c.nivel,"Basico");
    strcpy(c.descripcion, "Mucho Texto");
    strcpy(c.prof.nombre, "Jorge Gracia");
    strcpy(c.prof.direccion, "Su casa");
    c.prof.tel = 1234;
    strcpy(c.prof.dpto.nombre,"Tec. Programacion");
    strcpy(c.prof.dpto.director, "EL DIRECTOR");
    strcpy(c.prof.dpto.descripcion, "Mucho Texto");
    //MOSTRAR LAS ESTRUCTURAS
    printf("Departamento: %s - Director: %s - Descripcion: %s\n",c.prof.dpto.nombre,c.prof.dpto.director,c.prof.dpto.descripcion);
    printf("Profesor: %s - Direccion: %s - Tel: %d\n",c.prof.nombre,c.prof.direccion,c.prof.tel);
    printf("Curso: %s - Nivel: %s - Descripcion: %s\n",c.nombre,c.nivel,c.descripcion);
    return 0;
}