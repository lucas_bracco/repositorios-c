#include<stdio.h>

int main(){
    //Pedir número al usuario (entre 0 y 9999).
    //Que indique unidades, decenas, centenas y unidades de mil.
    int x,aux;
    printf("Ingrese un numero entre 0 y 9999\n");
    scanf("%d", &x);
    aux=x/1000;
    printf("Unidad de mil: %d\n",aux);
    aux=(x%1000)/100;
    printf("Centena: %d\n",aux);
    aux=((x%1000)%100)/10;
    printf("Decena: %d\n",aux);
    aux=((x%1000)%100)%10;
    printf("Unidades: %d\n",aux);
    return 0;
}