#include <stdio.h>

int main(){
    int vocales=0,consonantes=0,i=0;
    char cadena[90];
    printf("Ingresa un texto.\n");
    gets(cadena);

    while (cadena[i] != 0){

        if(64 < cadena[i] && cadena[i] < 91){
            if(cadena[i] == 65 || cadena[i] == 69 || cadena[i] == 73 || cadena[i] == 79 || cadena[i] == 85){
            vocales++;
            }else{
            consonantes++;
            }
        }

        if(96 < cadena[i] && cadena[i] < 123){
            if (cadena[i] == 97 || cadena[i] == 101 || cadena[i] == 105 || cadena[i] == 111 || cadena[i] == 117){
            vocales++;
            }else{
                consonantes++;
            }
        }

        i++;
    }

    printf("Cantidad de vocales: %d\nCantidad de consonantes: %d",vocales,consonantes);
    
    return 0;
}