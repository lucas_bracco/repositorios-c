#include<stdio.h>
#include<math.h>

float potencia(int);
float raiz(float);

int main(){
    int i;
    for(i=2;i<=10;i++){
        //EL IF Y EL ELSE SON SOLO PARA QUE SE VEA BIEN EN PANTALLA, NO APORTAN NADA MAS.
        if(i<7){        
            printf("La potencia de Pi elevado en %d es: %.8f\t\t",i,potencia(i));
            printf("Y su raiz es: %0.8f\n", raiz(potencia(i)));
        }else
        {
            printf("La potencia de Pi elevado en %d es: %.8f\t",i,potencia(i));
            printf("Y su raiz es: %0.8f\n", raiz(potencia(i)));
        }        
    }
    return 0;
}

float potencia(int a){
    return pow(M_PI,a); //M_PI ES LA CONSTANTE PI.
}

float raiz(float a){
    return sqrt(a);
}