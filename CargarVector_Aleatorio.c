#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//FUNCIONES PROTOTIPO.
void cargar ();
void mostrar ();
int cont_par ();
int cont_impar ();

//VARIABLES GLOBALES
int n,v[999];

int main(){
    printf("Ingrese un numero entero para definir el tamanio de su vector.\n");
    scanf("%d",&n);
    cargar(n);
    mostrar(n);
    printf("Cantidad de numeros par:\t%d\nCantidad de numeros impar:\t%d",cont_par(n),cont_impar(n));
    return 0;
}

//FUNCION PARA CARGAR EL VECTOR CON NUMEROS ALEATORIOS.
void cargar (){
    int i;
    srand(time(0));
    for(i=0;i<n;i++){
        v[i]=rand();
    }
}

//FUNCION PARA MOSTRAR VECTOR.
void mostrar (){
    int i;
    printf("Vector:\n");
    for(i=0;i<n;i++){
        printf("%d\t%d\n",i,v[i]);
    }
}

//FUNCION PARA CONTAR CUANTOS NUMEROS PAR HAY.
int cont_par (){
    int i,c=0;
    for(i=0;i<n;i++){
        if(v[i]%2==0){ //SI EL RESTO DEL NUMERO GUARDADO EN EL VECTOR DIVIDO EN 2 ES 0, EN NUMERO ES PAR Y SUMA 1 EL ACUMULADOR "c".
            c++;
        }
    }
    return c;
}

//FUNCION PARA CONTAR CUANTOS NUMEROS IMPAR HAY.
int cont_impar (){
    int i,c=0;
    for(i=0;i<n;i++){
        if(v[i]%2==1){ //SI EL RESTO DEL NUMERO GUARDADO EN EL VECTOR DIVIDO EN 2 ES 1, EN NUMERO ES imPAR Y SUMA 1 EL ACUMULADOR "c".
            c++;
        }
    }
    return c;
}