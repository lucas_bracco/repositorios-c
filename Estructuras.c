#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
    int a;      //4 bytes.
    float b;    //4 bytes.
    char c[70]; //70 bytes.
}Estructura;    //70 + 4 + 4 = 78 bytes.


int main(){
    Estructura z;
    z.a = 10;
    z.b = 4.5;
    strcpy(z.c,"texto");
    printf("%d\n", sizeof(z));
    printf("%d\n", z.a);
    printf("%.2f\n", z.b);
    printf("%s\n", z.c);
    return 0;
}