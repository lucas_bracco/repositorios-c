#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int main(){
    char a = 'a';
    char *p;

    p = &a;

    *p = 40;//Se modifica indirectamente el valor de la variable a.

    printf("Valor del Puntero: %p\n", p);//%p: Permite mostrar dirección de memoria.

    printf("Direccion de memoria de a: %p\n", &a);//&: Dirección de Variable.

    printf("Valor de la variable apuntada: %c\n", *p);//*: Contenido de la variable apuntada.

    printf("Direccion de memoria del puntero: %p\n", &p);

    return 0;
}