#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

typedef struct
{
    float real;
    float imag;
}Complejos;

void mostrar_complejos(Complejos a, char c[]);

Complejos sumatoria(Complejos a, Complejos b);
Complejos multiplicacion(Complejos a, Complejos b);
Complejos division(Complejos a, Complejos b);

int main(){

    Complejos x,y,z;

    printf("Ingrese el valor real de X.\n");
    scanf("%f", &x.real);
    printf("Ingrese el valor imaginario de X.\n");
    scanf("%f", &x.imag);
    printf("Ingrese el valor real de Y.\n");
    scanf("%f", &y.real);
    printf("Ingrese el valor imaginario de Y.\n");
    scanf("%f", &y.imag);
    system("cls");

    mostrar_complejos(x,"X");
    mostrar_complejos(y,"Y");

    z = sumatoria(x,y);
    printf("Suma\n");
    mostrar_complejos(z,"Z");

    z = multiplicacion(x,y);
    printf("Multiplicacion\n");
    mostrar_complejos(z,"Z");

    z = division(x,y);
    printf("Division\n");
    mostrar_complejos(z,"Z");

    return 0;
}
//FUNCIONES.
void mostrar_complejos(Complejos a, char c[]){
    printf("%s = %.3f %+.3fi\n",c,a.real,a.imag);
}

//---------------------
Complejos sumatoria(Complejos a, Complejos b){
    Complejos z;
    z.real = a.real + b.real;
    z.imag = a.imag + b.imag;
    return z;
}

//----------------------//
Complejos multiplicacion(Complejos a, Complejos b){
    Complejos z;
    z.real = a.real * b.real - a.imag * b.imag;
    z.imag = a.real * b.imag + a.imag * b.real;
    return z;
}

//----------------------
Complejos division(Complejos a, Complejos b){
    Complejos z;
    z.real = (a.real * b.real + a.imag * b.imag) / (pow(b.real,2) + pow(b.imag,2));
    z.imag = (a.imag * b.real - a.real * b.imag) / (pow(b.real,2) + pow(b.imag,2));
    return z;
}