#include<stdio.h>
#include<limits.h>
int main(){
    printf("Variable\tTamanio[bytes]\tminimo\t\t\tMaximo\n");
    //ENTEROS
    printf("Char\t\t%lu\t\t%d\t\t\t%d\n",sizeof(char),CHAR_MIN,CHAR_MAX);//%lu=Unsigned long / %ld=Decimal Largo sizeof devuelve el tamaño en bits de la variable.
    printf("short\t\t%lu\t\t%d\t\t\t%d\n",sizeof(short),SHRT_MIN,SHRT_MAX);
    printf("Int\t\t%lu\t\t%d\t\t%d\n",sizeof(int),INT_MIN,INT_MAX);
    printf("Long\t\t%lu\t\t%d\t\t%d\n",sizeof(long),LONG_MIN,LONG_MAX);
    printf("Long Long\t%lu\t%lld\t%lld\n",sizeof(long long),LLONG_MIN,LLONG_MAX);//%lld Permite mostrar números grandes. 
    //PUNTOS FLOTANTES
    printf("Float\t\t%lu\t\t%e\t\t%e\n",sizeof(float),__FLT_MIN__,__FLT_MAX__);//%e= Expresa los números con notación cientifica. EJ: 1,34e^38
    printf("Double\t\t%lu\t\t%e\t\t%e\n",sizeof(double),__DBL_MIN__,__DBL_MAX__);
    printf("Long Double\t%lu\t\t%e\t\t%e\n",sizeof(long double),__LDBL_MIN__,__LDBL_MAX__);
    return 0;
}