#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//ESTRUCTURAS.

typedef struct{
    char nombre[20];
    char director[20];
    char descripcion[100];
}Departamentos;

typedef struct{
    char nombre[20];
    char direccion[50];
    char tel[20];
}Profesores;

typedef struct{
    char nombre[50];
    char nivel[20];
    char descripcion[100];
}Cursos;

int main(){
    int i,j;
    Cursos c[2][2];
    Profesores p[3];
    Departamentos d;
    //CARGAR LAS ESTRUCTURAS.

    strcpy(d.nombre,"Tec. Programacion");
    strcpy(d.director, "EL DIRECTOR");
    strcpy(d.descripcion, "Mucho Texto");

    for(i=0;i<2;i++){
        for(j=0;j<2;j++){
            printf("Ingrese el nombre del curso [%d][%d]\n",i,j);
            gets(c[i][j].nombre);
            printf("Ingrese el nivel del curso [%d][%d]\n",i,j);
            gets(c[i][j].nivel);
            printf("Ingrese la descripcion del curso [%d][%d]\n",i,j);
            gets(c[i][j].descripcion);
            system("cls");
        }
    }

    for(i=0;i<3;i++){
        printf("Ingrese el nombre del profesor [%d]\n",i);
        gets(p[i].nombre);
        printf("Ingrese la direccion del profesor [%d]\n",i);
        gets(p[i].direccion);
        printf("Ingrese el telefono del profesor [%d]\n",i);
        gets(p[i].tel);
        system("cls");
    }    

    //MOSTRAR LAS ESTRUCTURAS

    printf("Departamento: %s - Director: %s - Descripcion: %s\n",d.nombre,d.director,d.descripcion);
    
    for(i=0;i<2;i++){
        for(j=0;j<2;j++){
            printf("Curso: %s - Nivel: %s - Descripcion: %s\n",c[i][j].nombre,c[i][j].nivel,c[i][j].descripcion);
        }
    }

    for(i=0;i<3;i++){
        printf("Profesor: %s - Direccion: %s - Tel: %d\n",p[i].nombre,p[i].direccion,p[i].tel);
    } 

    return 0;
}