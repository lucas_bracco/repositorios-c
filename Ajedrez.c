#include <stdio.h>

int main(){
    char tablero[8][8];
    for(int j=0;j<4;j++){
        if(j == 0){
            tablero[0][j] = 'T';
            tablero[7][j] = 'T';
            tablero[0][7-j] = 'T';
            tablero[7][7-j] = 'T';
            tablero[1][j] = 'P';
            tablero[1][7-j] = 'P';
            tablero[6][j] = 'P';
            tablero[6][7-j] = 'P';
        }else if(j == 1){
            tablero[0][j] = 'C';
            tablero[7][j] = 'C';
            tablero[0][7-j] = 'C';
            tablero[7][7-j] = 'C';
            tablero[1][j] = 'P';
            tablero[1][7-j] = 'P';
            tablero[6][j] = 'P';
            tablero[6][7-j] = 'P';
        }else if(j == 2){
            tablero[0][j] = 'A';
            tablero[7][j] = 'A';
            tablero[0][7-j] = 'A';
            tablero[7][7-j] = 'A';
            tablero[1][j] = 'P';
            tablero[1][7-j] = 'P';
            tablero[6][j] = 'P';
            tablero[6][7-j] = 'P';
        }else if(j == 3){
            tablero[0][j] = 'R';
            tablero[7][j] = 'R';
            tablero[0][j+1] = 'M';
            tablero[7][j+1] = 'M';
            tablero[1][j] = 'P';
            tablero[1][7-j] = 'P';
            tablero[6][j] = 'P';
            tablero[6][7-j] = 'P';
        }
    }

    for(int i=0;i<8;i++){
        for(int j=0;j<8;j++){
            printf("[%c]", tablero[i][j]);
        }
        printf("\n");
    }

    return 0;
}
