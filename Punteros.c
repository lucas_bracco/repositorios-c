#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

void invertir (char *origen, char *destino);

char orig[40];;
char dest[40];

int main(){
    printf("Ingrese una frase:\n");
    gets(orig);
    invertir(orig,dest);
    system("cls");
    printf("Orig: %s\n", orig);
    printf("Dest: %s\n", dest);
    return 0;
}

void invertir (char *origen, char *destino){
    int n=0;
    while(*origen != '\0'){
        n++;
        *origen++;
    }
    origen--;
    for(int i=0;i<n;i++){
        *destino++ = *origen--;
        if(i == n-1){
            *destino = '\0';
        }
    }
}